package com.adheus.jogodavea.controllers.tictacgame.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adheus.jogodavea.R;
import com.adheus.jogodavea.controllers.tictacgame.TicTacGameFragment.OnListFragmentInteractionListener;
import com.adheus.jogodavea.models.GameManager;
import com.adheus.jogodavea.models.TicTacCell;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * {@link RecyclerView.Adapter} that can display a {@link TicTacCell} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TicTacCellRecyclerViewAdapter extends RecyclerView.Adapter<TicTacCellRecyclerViewAdapter.ViewHolder> {

    private final GameManager gameManager;
    private final OnListFragmentInteractionListener mListener;

    private final int HEADER_VIEW_TYPE = 1;
    private final int CELL_VIEW_TYPE = 2;

    public TicTacCellRecyclerViewAdapter(GameManager gameManager, OnListFragmentInteractionListener listener) {
        this.gameManager = gameManager;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == HEADER_VIEW_TYPE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_tictac_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_tictac_cell, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (isHeader(position)) {
            holder.mIdView.setText(gameManager.getCurrentGameStatus().getStringDescription(holder.mIdView.getContext()));
            holder.mView.setOnClickListener(null);
            return;
        }

        TicTacCell currentCell = gameManager.getItemAtPosition(position-1);

        holder.mItem = currentCell;
        holder.mIdView.setText(currentCell.getCurrentState().toString());
        holder.mIdView.setVisibility(View.VISIBLE);

        holder.mImageView.setVisibility(View.INVISIBLE);
        String pictureURL = currentCell.getCurrentState().getPictureURL();
        if(pictureURL != null) {
            Picasso.with(holder.mImageView.getContext()).load(pictureURL).into(holder.mImageView, new Callback() {
                @Override
                public void onSuccess() {
                    holder.mIdView.setVisibility(View.INVISIBLE);
                    holder.mImageView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {

                }
            });
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.getAdapterPosition());

                }
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ?
                HEADER_VIEW_TYPE : CELL_VIEW_TYPE;
    }

    public void updateHeader() {
        this.notifyItemChanged(0);
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        //Plus one for Header
        return 1 + (GameManager.getInstance().getBoardSize() * GameManager.getInstance().getBoardSize());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final ImageView mImageView;
        public TicTacCell mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            //mContentView = (TextView) view.findViewById(R.id.content);
            mImageView = (ImageView) view.findViewById(R.id.cell_picture);

        }

        @Override
        public String toString() {
            return super.toString() + " - " + mIdView.getText();
        }
    }
}

package com.adheus.jogodavea.controllers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.adheus.jogodavea.R;
import com.adheus.jogodavea.controllers.tictacgame.TicTacGameFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final TicTacGameFragment ticTacGameFragment = new TicTacGameFragment();
        setMainContentFragment(ticTacGameFragment);

        findViewById(R.id.restart_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticTacGameFragment.restartGame();
            }
        });

    }

    public void setMainContentFragment(Fragment fragment) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame, fragment);
        transaction.commit();
    }
}

package com.adheus.jogodavea.models;

import android.content.Context;

import com.adheus.jogodavea.R;

/**
 * Created by adheus on 9/3/16.
 */
public class GameManager {

    public enum GameStatus {
        XsTurn, OsTurn, XWins, OWins, Tied;

        public String getStringDescription(Context context) {
            switch (this) {
                case XsTurn:
                    return context.getString(R.string.xs_turn);
                case OsTurn:
                    return context.getString(R.string.os_turn);
                case XWins:
                    return context.getString(R.string.x_wins);
                case OWins:
                    return context.getString(R.string.o_wins);
                case Tied:
                    return context.getString(R.string.tied);
                default:
                    return "";
            }
        }
    }


    private final static int BOARD_SIZE = 3;

    private int round;

    private TicTacCell[][] board;

    private GameStatus currentGameStatus;

    private static GameManager instance;

    private GameManager() {
        this.round = 1;
        this.board = this.generateBoard();
        this.currentGameStatus = this.checkGameStatus();
    }

    //Singleton
    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }

        return instance;
    }


    public void resetGame() {
        this.round = 1;
        this.board = this.generateBoard();
        this.currentGameStatus = this.checkGameStatus();
    }

    public int getCurrentRound() {
        return this.round;
    }

    public int getBoardSize() {
        return BOARD_SIZE;
    }

    private GameStatus checkGameStatus() {

        boolean xTurn = round % 2 != 0;

        //Did somebody win?
        if (didSomeoneWin()) {
            //If X's Turn than O's last move won the game.
            return (xTurn) ? GameStatus.OWins : GameStatus.XWins;
        } else if (round > (BOARD_SIZE * BOARD_SIZE)) {
            return GameStatus.Tied;
        }
        return (xTurn) ? GameStatus.XsTurn : GameStatus.OsTurn;
    }

    public GameStatus getCurrentGameStatus() {
        return this.currentGameStatus;
    }

    public boolean isGameOver() {
        return !(this.currentGameStatus == GameStatus.OsTurn || this.currentGameStatus == GameStatus.XsTurn);
    }

    public TicTacCell[][] getBoard() {
        return this.board;
    }

    private boolean didSomeoneWin() {

        TicTacCell[] diagonalArray = new TicTacCell[BOARD_SIZE];
        TicTacCell[] antiDiagonalArray = new TicTacCell[BOARD_SIZE];

        for (int i = 0; i < BOARD_SIZE; i ++) {
            //Test row array
            if (isAWinSituation(this.board[i])) {
                return true;
            }

            TicTacCell[] columnArray = new TicTacCell[BOARD_SIZE];
            for (int j = 0; j < BOARD_SIZE; j++) {
                columnArray[j] = this.board[j][i];
                if (i == j) {
                    diagonalArray[i]  = this.board[i][j];
                }
                if (i + j == (BOARD_SIZE - 1)) {
                    antiDiagonalArray[i] = this.board[i][j];
                }
            }
            if(isAWinSituation(columnArray)) {
                return true;
            }
        }
        return isAWinSituation(diagonalArray) || isAWinSituation(antiDiagonalArray);
    }

    private boolean isAWinSituation(TicTacCell[] ticTacCells) {
        //Check if there is empty spaces
        int cellSum = 0;

        for (int i = 0; i < ticTacCells.length; i++) {
            if (ticTacCells[i].getCurrentState() == TicTacCell.CellState.Empty)
                return false;

            cellSum += ticTacCells[i].getCurrentState().getValue();
        }

        return cellSum == (BOARD_SIZE * TicTacCell.CellState.O.getValue())
                    || cellSum == (BOARD_SIZE * TicTacCell.CellState.X.getValue());

    }



    public TicTacCell makeMove(int position) {
        return this.changeCellState(getItemAtPosition(position));
    }

    public TicTacCell changeCellState(TicTacCell ticTacCell) {

        if (ticTacCell.getCurrentState() != TicTacCell.CellState.Empty) {
            return ticTacCell;
        }

        boolean xTurn = round % 2 != 0;
        if (xTurn) {
            ticTacCell.setCurrentState(TicTacCell.CellState.X);
        } else {
            ticTacCell.setCurrentState(TicTacCell.CellState.O);
        }

        this.round++;
        this.currentGameStatus = checkGameStatus();

        return ticTacCell;
    }

    public int[] getCoordinatesFromPosition(int position) {
        int firstPosition = position / GameManager.BOARD_SIZE;
        int secondPosition = position - (firstPosition * GameManager.BOARD_SIZE);
        return new int[] { firstPosition, secondPosition };
    }

    public TicTacCell getItemAtPosition(int position) {
        int[] coordinates = getCoordinatesFromPosition(position);
        return this.board[coordinates[0]][coordinates[1]];
    }


    private TicTacCell[][] generateBoard() {
        TicTacCell[][] board = new TicTacCell[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < BOARD_SIZE; i++) {
            for(int j = 0; j < BOARD_SIZE; j++) {
                board[i][j] = new TicTacCell();
            }
        }

       return board;
    }


}

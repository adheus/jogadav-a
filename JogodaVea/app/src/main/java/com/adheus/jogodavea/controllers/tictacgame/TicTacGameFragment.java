package com.adheus.jogodavea.controllers.tictacgame;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adheus.jogodavea.R;
import com.adheus.jogodavea.controllers.tictacgame.adapters.TicTacCellRecyclerViewAdapter;
import com.adheus.jogodavea.models.GameManager;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TicTacGameFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;


    private TicTacCellRecyclerViewAdapter currentAdapter;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TicTacGameFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tictac_board, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setHasFixedSize(true);
            final GridLayoutManager manager = new GridLayoutManager(context, GameManager.getInstance().getBoardSize());
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (currentAdapter != null && currentAdapter.isHeader(position)) {
                        return manager.getSpanCount();
                    }
                    return 1;
                }
            });
            recyclerView.setLayoutManager(manager);
            this.currentAdapter = new TicTacCellRecyclerViewAdapter(GameManager.getInstance(), mListener);
            recyclerView.setAdapter(currentAdapter);
        }
        return view;
    }


    public void restartGame() {
        GameManager.getInstance().resetGame();
        if (this.currentAdapter != null) {
            this.currentAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(int position) {
                //Exclude header shift
                if (GameManager.getInstance().isGameOver()) {
                    Toast.makeText(TicTacGameFragment.this.getContext(), R.string.game_over_message, Toast.LENGTH_SHORT).show();
                    return;
                }
                GameManager.getInstance().makeMove(position-1);

                //Notify UI new game conditions
                TicTacGameFragment.this.currentAdapter.notifyItemChanged(position);
                TicTacGameFragment.this.currentAdapter.updateHeader();
            }
        };

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.currentAdapter = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(int position);
    }
}

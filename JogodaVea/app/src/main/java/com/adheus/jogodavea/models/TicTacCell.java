package com.adheus.jogodavea.models;

/**
 * Created by adheus on 9/3/16.
 */
public class TicTacCell {

    public static final String XS_PICTURE_URL = "http://www.adheus.com/wp-content/uploads/2016/09/xs_look.png";
    public static final String OS_PICTURE_URL = "http://www.adheus.com/wp-content/uploads/2016/09/os_look.png";

    public enum CellState {
        O(3), X(1), Empty(0);

        private final int value;
        CellState(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public String toString() {
            switch (this.value) {
                case 0:
                    return "";
                case 1:
                    return "X";
                case 3:
                    return "O";
                default:
                    return "";
            }
        }

        public String getPictureURL() {
            switch (this.value) {
                case 1:
                    return XS_PICTURE_URL;
                case 3:
                    return OS_PICTURE_URL;
                default:
                    return null;
            }
        }
    }



    private CellState currentState = CellState.Empty;

    public CellState getCurrentState() {
        return this.currentState;
    }
    public void setCurrentState(CellState state) {
        this.currentState = state;
    }
}
